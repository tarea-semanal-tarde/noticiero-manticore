# Tecnología

En esta sección se podrá observar noticias sobre los siguientes temas de interés tecnológico.

* [Apps](#sobre-apps)
* [Dispositivos](#sobre-dispositivos)
* [Varios](#sobre-noticias-varias)

## Sobre Apps

Aquí se discuten las noticias más importantes acerca de **aplicaciones** desarrolladas para diferentes plataformas.
 
  
### El navegador chino

> El navegador "100% Chino" es una copia íntegra del navegador americano "Google Chrome". El navegador desarrollado en China se llama RedCore y la compañia desarrolladora ya ha pedido disculpas. *lee más en el siguiente [enlace](https://elpais.com/tecnologia/2018/08/17/actualidad/1534496744_756231.html)*

![Navegador Chino](https://ep01.epimg.net/tecnologia/imagenes/2018/08/17/actualidad/1534496744_756231_1534496895_noticia_normal_recorte1.jpg)


### Instagram y Facebook implementan herramientas para controlar el tiempo de uso.
> Las nuevas utilidades permiten conocer cuántas horas se emplean, establecer límites y recibir alertas de abuso.*Lee más en el siguiente [enlace](https://elpais.com/tecnologia/2018/08/14/actualidad/1534242030_729041.html)*

![Instagram y Facebook](https://ep01.epimg.net/tecnologia/imagenes/2018/08/14/actualidad/1534242030_729041_1534242202_noticia_normal_recorte1.jpg)

## Sobre Dispositivos

Aquí se discuten las noticias más importantes acerca de **dispositivos** que son hechos por diferentes compañías dedicadas al desarrollo de tecnología.

### Xiaomi prepara una segunda marca de teléfonos móviles

>El próximo día 22 Xiaomi lanzará en India el primer terminal de una segunda marca: Poco. En Facebook y Twitter se pudo apreciar el siguiente mensaje.
>> Queremos hacer un móvil potente con las tecnologías que de verdad importan

![Xiaomi](https://ep01.epimg.net/tecnologia/imagenes/2018/08/14/actualidad/1534255343_855716_1534322521_noticia_normal_recorte1.jpg)

### Altavoces inteligentes

> Un vistazo por los altavoces inteligentes creados por las diferentes empresas entre ellas Amazon, Google, Apple y ahora también Samsung. Hoy en día se decide que va a ser de ellos.
*Lee más en el siguiente [enlace](https://elpais.com/tecnologia/2018/08/14/actualidad/1534266263_026752.html)*

![Altavoces inteligentes](https://ep01.epimg.net/tecnologia/imagenes/2018/08/14/actualidad/1534266263_026752_1534266385_noticia_normal_recorte1.jpg)

## Sobre Noticias Varias

Aquí se discuten **noticias relevantes** en el campo de la Tecnología que no son software ni hardware.


### Artículos tecnológicos recomendados para comprar.
> Se pueden comprar varios artículos en diferentes tiendas online. Si sabemos buscar bien, se encuentran precios muy competitivos de dispositivos y otras adquisiciones que podremos seguir utilizando cuando llegue el frío. *Lee más en el siguiente [enlace](https://elpais.com/elpais/2018/08/16/escaparate/1534429586_985496.html)*

![Artículos tecnológicos recomendados](https://ep01.epimg.net/elpais/imagenes/2018/08/16/escaparate/1534429586_985496_1534434035_noticia_normal_recorte1.jpg)


## Videoteca

Aquí se muestran algunos videos relacionados con los temas anteriores. Haga clic en cualquiera de las siguientes imágenes para reproducir el video.

### Video sobre los altavoces inteligentes

[![Altavoces Video](https://img.youtube.com/vi/K_sIMt26wa4/0.jpg)](https://www.youtube.com/watch?v=K_sIMt26wa4)

### Video Xiaomi Pocophone F1 (Unboxing)
[![Xiaomi Poco Video](https://img.youtube.com/vi/Ci0v4vMcIDA/0.jpg)](https://www.youtube.com/watch?v=Ci0v4vMcIDA)

### El navegador chino
[![Navegador Chino Video](https://img.youtube.com/vi/AR4cGuIgkeY/0.jpg)](https://www.youtube.com/watch?v=AR4cGuIgkeY)
