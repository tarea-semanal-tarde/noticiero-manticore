# **TECNOLOGÍA** :computer:

# Bienvenidos a las noticias más relevantes acerca de tecnología.

![Logo Tecnología](http://aecsolutions.info/wp-content/uploads/2017/06/nuevas-tecnologias.jpg)

La tecnología es según wikipedia:
>La tecnología es la ciencia aplicada a la resolución de problemas concretos. Constituye un conjunto de conocimientos científicamente ordenados, que permiten diseñar y crear bienes o servicios que facilitan la adaptación al medio ambiente y la satisfacción de las necesidades esenciales y los deseos de la humanidad.

![Segunda Imagen](https://admin.kienyke.com/wp-content/uploads/2017/10/tecnologia.png)

En esta sección podremos ver noticias de tecnología en general. Debido a que el desarrollo de tecnología en Ecuador es muy escaso hemos decidido que todas las noticias de este tópico se mostrarán dando clic en el siguiente enlace.

### **[Noticias Tecnológicas](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Tecnolog%C3%ADa/TECNOLOGIA.md)**