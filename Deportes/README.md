# DEPORTES

**Bienvenidos**, las principales noticias deportivas a nivel nacional e internacional.

![git deportes](https://i.gifer.com/Unry.gif)

 **DEPORTE**. Para algunos es una forma de diversión y pasarlo bien, por ejemplo ir a ver un partido de baloncesto o fútbol o practicarlo, ir a jugar al tenis un sábado por la tarde o a montar en bici con amigos.

El deporte es, entonces, un medio para disfrutar de la vida, para mejorar la salud y, a la vez, una forma de ganarse la vida ya sea de forma profesional o no.

ir a la pagina de noticias [*Deportes*](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Deportes/DEPORTES.md)


