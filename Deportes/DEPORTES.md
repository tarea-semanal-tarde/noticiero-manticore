# ***NOTICIAS NACIONALES E INTERNACIONES DEPORTIVAS***

 [NOTICIAS DEPORTIVAS NACIONALES](#NOTICIAS-DEPORTIVAS-NACIONALES) | [NOTICIAS DEPORTIVAS INTERNACIONALES](#NOTICIAS-DEPORTIVAS-INTERNACIONALES) | [PARTIDOS PARA EL FIN DE SEMANA](#PARTIDOS-PARA-EL-FIN-DE-SEMANA)
 --- | --- | ---

--------------------------
***PARA VER MAS SOBRE LA NOTICIA DAR CLIC EN LAS LETRAS AZULES.***

## ***NOTICIAS DEPORTIVAS NACIONALES***

Ricardo Vasconcellos: Las carreteras de la muerte y el [duelo en la hinchada torera](https://www.eluniverso.com/deportes/2018/08/19/nota/6911579/carreteras-muerte-duelo-hinchada-torera)

![LUTO BARCELONA](https://www.eluniverso.com/sites/default/files/styles/powgallery_800/public/fotos/2018/08/hinchas_fallecidos_img_20180815_104532_25691975.jpg?itok=0U69BCYo)


[Independiente ante Delfín](https://www.eluniverso.com/deportes/2018/08/19/nota/6911585/independiente-ante-delfin-duelo-ganadores), duelo de ganadores

![IDV vs DELFIN](https://www.eluniverso.com/sites/default/files/styles/powgallery_800/public/fotos/2018/08/14001469.jpg?itok=KCzL6YUI)

[Carlos Villacís:](https://www.elcomercio.com/deportes/carlosvillacis-copaamerica-bolivia-ecuador-futbol.html) Nos oponemos a que nos quiten la sede de la Copa América 2023 

![SEDE COPA AMERICA](https://www.elcomercio.com/files/article_main/uploads/2018/08/16/5b75e8ce51302.jpeg)

[‘Bolillo’ Gómez](https://www.elcomercio.com/deportes/bolillogomez-rejuvenece-seleccion-ecuador-futbol.html) ­rejuvenece a la Selección de Ecuador en su primera lista 

![BOLILLO GOMEZ](https://www.elcomercio.com/files/article_main/uploads/2018/08/16/5b75fb12bf5f9.jpeg)

[Ciclista quiteño Felipe Endara](https://www.elcomercio.com/deportes/ciclista-quito-felipeendara-coma-accidente.html) salió del estado de coma 

![CICLISTA](https://www.elcomercio.com/files/article_main/uploads/2018/07/26/5b5a3ea87a318.jpeg)

## ***NOTICIAS DEPORTIVAS INTERNACIONALES***

[Después de casi 18 años](https://somosinvictos.com/2018/08/15/conclusiones-invictas-despues-de-casi-18-real-madrid-perdio-una-final-internacional-lectura-obligatoria/), Real Madrid perdió una final internacional; Atleti se agigantó en Estonia. LECTURA OBLIGATORIA

![ATLETICO VS MADRID](https://i0.wp.com/somosinvictos.com/wp-content/uploads/2018/08/SR.jpg?resize=770%2C362&ssl=1)

[**EL MAGO BRASILEÑO:**](https://somosinvictos.com/2018/08/18/el-mago-brasileno-los-impresionantes-numeros-de-coutinho-en-sus-primeros-8-meses-barcelona/) Los impresionantes números de Philippe Coutinho en sus primeros 8 meses con el FC Barcelona.

![COUNTINHO](https://i1.wp.com/somosinvictos.com/wp-content/uploads/2018/08/Coutinho.jpg?resize=770%2C362&ssl=1)

[LA POSESIÓN NO CUENTA:](https://somosinvictos.com/2018/08/17/la-posesion-no-cuenta-la-interesante-reflexion-de-jurgen-klopp-sobre-el-juego-de-posesion-en-el-futbol/) La interesante reflexión de Jürgen Klopp sobre el juego de posesión en el fútbol

![JURGEN KLOOP](https://i2.wp.com/somosinvictos.com/wp-content/uploads/2018/08/Klopp-2.jpg?resize=770%2C362&ssl=1)

[NO DIGAN MENTIRAS:](https://somosinvictos.com/2018/08/17/mourinho-la-respuesta-de-jose-mourinho-para-los-que-dicen-que-su-relacion-con-pogba-esta-rota/) La respuesta de José Mourinho para los que sostienen que tuvo una pelea con Paul Pogba

![MOU](https://i0.wp.com/somosinvictos.com/wp-content/uploads/2018/08/Pogba.jpg?resize=770%2C362&ssl=1)

[**LO PONEN FUERA DEL ATLETI:**](https://somosinvictos.com/2018/08/18/lo-ponen-fuera-atleti-el-club-al-que-se-marcharia-filipe-luis-antes-de-que-finalice-el-mercado-veraniego/) El club al que se marcharía Filipe Luís antes de que finalice el mercado veraniego.

![FELIPE LUIS](https://i0.wp.com/somosinvictos.com/wp-content/uploads/2018/08/Filipe.jpg?resize=770%2C362&ssl=1)




## ***PARTIDOS PARA EL FIN DE SEMANA***

**SERIE A(ECUADOR)** | **LA LIGA(ESPAÑA**) | **PREMIER LEAGUE(INGLATERRA)** | **SERIE A(ITALIA)** |
-------- | ------ | ------ | ----- |
SABADO 18/08 | SABADO 18/08 | SABADO 18/08 | SABADO 18/08 |
LDU vs TECNICO UNIVERSITARIO | CELTA DE VIGO vs RCD ESPANYOL | CARDIFF vs NEWCASTLE | CHIEVO vs JUVENTUS|
BARCELONA vs UNIVERSIDAD CATOLICA | VILLAREAL CF vs REAL SOCIEDAD | EVERTON vs SOUTHAMPTOM | LAZIO vs NAPOLI|
DOMINGO 19/08  | FC BARCELONA vs ALAVES | TOTTENHAM vs FULHAM  | DOMINGO 19/08  
DELFIN vs IDV  | DOMINGO 19/08 | WEST HAM vs BOURNEMOUTH | LEICESTER vs WOLVES | SAMPDORIA vs FIORENTINA |
EMELEC vs DEP CUENCA  | RAYO VALENCIANO vs SEVILLA FC | CHELSEA vs ARSENAL | TORINO vs AS ROMA |
--- | REAL MADRID vs GETAFE CF | DOMINGO 19/08 | BOLOGNA vs SPAL |
-- | SD EIBAR vs HUESCA | BURNLEY vs WATFORD | EMPOLI vs CAGILARI |
-- | -- | BRIGHTON vs MANCHESTER UNITED | PARMA vs UDINENSE |
-- | -- | MANCHESTER CITY vs HUDDERSFIELD  | SASSOULO vs INTER MILAN

