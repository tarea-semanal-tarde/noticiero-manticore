# **Educación** :book:
Bienvenidos, en esta sección encontrarán noticias a nivel nacional como internacional relacionadas con la educación.

![Img-edu](https://www.ucn.cl/wp-content/uploads/2014/05/PedagogiaMatematicaEducacionMedia01a-1210x550.jpg)

*La Educación es un derecho fundamental reconocido en la Declaración Universal de los Derechos Humanos de 1948 y en la Convención sobre los Derechos del Niño  de 1989. La educación es para una convivencia en sociedad armónica, con sus semejantes y ser participe activo para la mejora de la misma.*

![Img-educacion](https://upload.wikimedia.org/wikipedia/commons/d/d3/AF-kindergarten.jpg)

## **Contenido**
Este repositorio contiene las siguientes noticias:
* [Educacion - Nacional :ec:] (https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/master/Educaci%C3%B3n/Educaci%C3%B3n-Nacional)
* [Educacion - Internacional :earth_americas:] (https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/master/Educaci%C3%B3n/Educaci%C3%B3n-Internacional)