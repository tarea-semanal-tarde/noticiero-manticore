# **Ecuador y Colombia acuerdan becas y asistencia en formación técnica y tecnológica**
***Jueves, 22 de febrero de 2018***

La Secretaría de Educación Superior, Ciencia, Tecnología e Innovación (Senescyt) y el Instituto Colombiano de Crédito Educativo y Estudios Técnicos en el Exterior se comprometieron en implementar la quinta convocatoria del programa de becas de reciprocidad, que ofrecerá 80 becas para que ciudadanos colombianos y ecuatorianos realicen sus estudios de maestría.

![img-educacion](https://www.eluniverso.com/sites/default/files/styles/powgallery_1024/public/fotos/2018/02/thinkstockphotos-178470111_800_440_80_c1.jpg?itok=mvecneW-)

El objetivo es brindar financiamiento que permita a los estudiantes formarse en instituciones de educación superior de Colombia o Ecuador, potenciando así integración e interculturalidad entre los dos países, la investigación, la ciencia, la tecnología y el intercambio de conocimientos.

Este compromiso se dio durante el VI Gabinete Binacional que se realizó la semana pasada en Pereira (Colombia) en el que además se abordaron temas relacionados con proyectos de cultura, salud, electrificación, biodiversidad, entre otros.

**Fuente:** [El universo](https://www.eluniverso.com/noticias/2018/02/22/nota/6634846/ecuador-colombia-acuerdan-becas-asistencia-formacion-tecnica)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Internacional)