# **Francia Prohíbe usar celulares en las escuelas**
***Lunes, 30 de julio de 2018***

El Parlamento francés adoptó este lunes de forma definitiva la prohibición de los teléfonos celulares en los centros de educación primaria y secundaria, una promesa de campaña del presidente Emmanuel Macron.

Los diputados de la mayoría presidencial y del centro votaron a favor de este texto durante su votación definitiva en la Asamblea Nacional, la cámara baja del Parlamento, mientras que la derecha y la izquierda se abstuvieron, criticando una "operación de comunicación" que "no va a cambiar nada".

![img-celular](https://cdn.oem.com.mx/elsoldedurango/2017/01/LA-LEGISLADORA-1A.jpg)

Este proyecto de ley prohíbe el uso de todo aparato conectado (móviles, tabletas, relojes) en las escuelas y colegios, es decir los centros de educación secundaria que por lo general acogen a niños de hasta 15 años.

Habrá excepciones "para el uso pedagógico", así como para los niños dispacacitados, se anunció.

En lo que concierne a los cursos superiores (entre 15 y 18 años), cada instituto podrá decidir si adopta o no la medida, y si lo hace de forma parcial o total.

La ley francesa prohíbe desde 2010 los teléfonos celulares "durante cualquier actividad de enseñanza y en los lugares (donde están) previstos por un reglamento interior".

El ministro de Educación, Jean-Michel Blanquer, que estimaba que esto no se cumplía plenamente, aplaudió esta nueva ley "de entrada al siglo 21" y que "envía un mensaje a la sociedad francesa", pero también al extranjero, donde "otros países han mostrado su interés".

En el capítulo dedicado a la Educación del programa electoral de Macron, elegido presidente en mayo de 2017, figuraba la prohibición de los celulares en las escuelas de primaria y secundaria.

**Fuente:** [El universo](https://www.eluniverso.com/noticias/nota/6883331/francia-prohibe-usar-celulares-escuelas)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Internacional)