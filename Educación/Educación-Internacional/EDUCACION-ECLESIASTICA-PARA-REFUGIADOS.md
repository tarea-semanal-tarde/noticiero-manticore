# **Educación eclesiástica para los refugiados, determinó el papa Francisco**
***Lunes, 29 de enero de 2018***

El papa Francisco aprobó este lunes una nueva Constitución Apostólica llamada Veritatis Gaudium sobre las universidades eclesiásticas. Este documento, que actualiza el aprobado por Juan Pablo II hace 39 años, que busca relanzar los estudios eclesiásticos hacia una transformación misionera, según Europa Press.

Uno de los puntos que trata es la posibilidad de que refugiados y solicitantes de asilo que no tengan documentos, puedan acceder a la educación en las universidades eclesiásticas. “La Facultad determinará en sus estatutos los procedimientos para evaluar las modalidades de tratamiento en el caso de refugiados, solicitantes de asilo o personas en situaciones análogas desprovistos de la regular documentación exigida”.

**Fuente:** [El universo](https://www.eluniverso.com/guayaquil/2018/01/30/nota/6591856/educacion-eclesiastica-refugiados)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Internacional)