# **Uno de cada cinco niños del mundo no va a la escuela: el desafío de la Unesco para 2030**
***Sábado, 3 de marzo de 2018***

Unos 263 millones de menores de edad, uno de cada cinco en todo el mundo, no van a la escuela. No solo el número es preocupante: lo es el hecho de que la cifra apenas ha cambiado en los últimos cinco años aun teniendo en cuenta el crecimiento general de la población, según un nuevo informe de la Unesco (Organización de las Naciones Unidas para la Educación, la Ciencia y la Cultura)

![img-niños-escuela](https://www.eluniverso.com/sites/default/files/styles/powgallery_1024/public/fotos/2018/03/fotonoticia_20180303144943_1920.jpg?itok=8_L6T0Ov)

En el nivel primario, el nueve por ciento de los niños de entre seis y 11 años, o 63 millones, no asisten a la escuela. 61 millones de personas de 12 a 14 años y 139 millones entre las edades de 15 a 17 años, uno de cada tres, no están inscriptos en la escuela. El grupo de mayor edad tiene cuatro veces más probabilidades de estar fuera de la escuela que los niños de edad primaria, y más del doble de probabilidades de estar fuera de la escuela que los de menor edad de secundaria.

En el África subsahariana, uno de cada tres niños, adolescentes y jóvenes no asisten a la escuela y las niñas tienen más probabilidades de ser excluidas. Por cada 100 niños en edad escolar que no asisten a la escuela, a 123 niñas se les niega el derecho a la educación.

Los datos también resaltan un abismo entre las tasas de deserción escolar en los países más pobres y ricos del mundo: el 59 % de los jóvenes de secundaria superior de todos los países de bajos ingresos no asisten a la escuela, comparado con el 6 % de los más países con más altos ingresos.

**Fuente:** [El universo](https://www.eluniverso.com/noticias/2018/03/03/nota/6649282/cada-cinco-ninos-mundo-no-va-escuela-desafio-unesco-2030)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Internacional)