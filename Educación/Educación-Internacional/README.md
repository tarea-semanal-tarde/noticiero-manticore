# **Noticias internacionales con temática educativa** :pencil:
Disfruta del diverso contenido que tenemos para ti:
## Contenido
* [Francia Prohíbe usar celulares en las escuelas](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Internacional/FRANCIA-PROHIBE-CELULARES.md)
* [China encarcela a 6 personas por hacer trampa  en examen de graduación](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Internacional/TRAMPA-EN-EXAMEN-DE-GRADUACION.md)
* [Ecuador y Colombia acuerdan becas y asistencia en formación técnica y tecnológica](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Internacional/ECUADOR-Y-COLOMBIA-ACUERDAN-BECAS.md)
* [Uno de cada cinco niños del mundo no va a la escuela: el desafío de la Unesco para 2030](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Internacional/UNO-DE-CADA-CINCO-NI%C3%91OS-NO-VA-A-LA-ESCUELA.md)
* [Educación eclesiástica para los refugiados, determinó el papa Francisco](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Internacional/EDUCACION-ECLESIASTICA-PARA-REFUGIADOS.md)

## [Ir a noticias nacionales acerca de la educación](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/master/Educaci%C3%B3n/Educaci%C3%B3n-Nacional)