# **China encarcela a 6 personas por hacer trampa  en examen de graduación**
***Miércoles, 8 de agosto de 2018***

Seis personas han sido encarceladas y podrían recibir penas de hasta cuatro años por participar de un plan para hacer trampas en el examen nacional de graduación, informó la prensa estatal el miércoles.

![img-noticia](https://www.eluniverso.com/sites/default/files/styles/powgallery_800/public/fotos/2018/08/examen_china_08082018.jpg?itok=96W8Bti3)

Los examinados, provistos de transmisores y receptores inalámbricos, debían leer las preguntas en voz alta, según los informes. Los cómplices fuera del lugar buscaban las respuestas en los libros de texto y se las leían.

Dos de los sentenciados el martes habían ayudado a reclutar clientes. No estaba claro cuánto pagaban por el servicio ni cuál sería su castigo. La sanción habitual es la descalificación perpetua de los exámenes.

Los seis fueron condenados a entre 20 meses y cuatro años de prisión y multas de hasta 40.000 yuan (5.900 dólares).

El puntaje obtenido en los exámenes es el criterio fundamental para poder avanzar en el sistema educativo chino desde los primeros niveles.

**Fuente:** [El universo](https://www.eluniverso.com/noticias/2018/08/08/nota/6896759/china-encarcela-6-personas-hacer-trampa-examen-graduacion)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Internacional)