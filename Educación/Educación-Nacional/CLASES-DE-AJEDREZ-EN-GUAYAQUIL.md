# **Clases gratuitas de ajedrez en la terminal terrestre de Guayaquil**
***Lunes, 6 de agosto de 2018***

El centro comercial Terminal de Guayaquil promueve el programa Pequeños Genios del Ajedrez. Las jornadas tienen como objetivo crear una réplica positiva en todos los niños y adolescentes entre 6 a 12 años que practican o desean aprender ajedrez y desarrollar sus tácticas y estrategias.

![img-ajedrez](https://www.deporte.gob.ec/wp-content/uploads/2015/08/20150825GuayasMonarcaAjedrez.jpg)

El proyecto incluye clases gratuitas a cargo de la ajedrecista y campeona mundial Evelyn Moncayo que ayudará a un sinnúmero de interesados en esta disciplina con su desarrollo intelectual y practica de valores en la sociedad.

Además, los participantes, turistas y ciudadanía podrán deleitarse todos los sábados de clases personalizadas junto a la experta que en un tablero gigante brindará consejos y tips para ser un experto.

**Fuente:** [El universo](https://www.eluniverso.com/guayaquil/2018/08/07/nota/6894374/clases-gratuitas-ajedrez)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Nacional)