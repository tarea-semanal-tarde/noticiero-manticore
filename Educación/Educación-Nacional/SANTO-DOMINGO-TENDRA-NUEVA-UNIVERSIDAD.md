# **Santo Domingo tendrá nueva universidad después de 2019**
***Sábado, 11 de agosto de 2018***

La necesidad de diversificar los campos de conocimiento así como suplir la demanda de los jóvenes en lugares con alto porcentaje poblacional son los principales argumentos del Gobierno para crear más universidades: en el norte y sur de la Amazonía y una en Santo Domingo de los Tsáchilas.

La creación de las universidades en Santo Domingo de los Tsáchilas y en la Amazonía se viabilizó a través de una Ley Reformatoria a la Ley Orgánica de Educación Superior.

![img-santo-domingo-universidad](https://www.eluniverso.com/sites/default/files/styles/powgallery_1024/public/fotos/2018/08/13925388.jpg?itok=ivt3AeTn)

La normativa dispone que, para garantizar la igualdad de oportunidades y el acceso a la educación superior, el Gobierno promoverá la creación de universidades o escuelas politécnicas en Santo Domingo de los Tsáchilas y en el norte y sur de la región amazónica.

De acuerdo con los cálculos del Gobierno, el proyecto de ley para la creación de la Universidad de Santo Domingo sería remitido a la Asamblea a finales de 2019. Mientras que para las universidades amazónicas no existe una fecha tentativa, pues el plan previo del Gobierno es el fortalecimiento de las sedes y extensiones ya existentes.

**Presupuesto**

Para las extensiones en la Amazonía se tiene previsto invertir $ 10 millones entre el 2019 y el 2021. Para la Universidad Amawtay Wasi y la Universidad de Santo Domingo, se prevé un presupuesto de $ 6 millones anuales. Conforme crezcan esos centros, sus presupuestos también lo harán. 

**Fuente:** [El universo](https://www.eluniverso.com/noticias/2018/08/11/nota/6899798/santo-domingo-tendra-nueva-universidad-despues-2019)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Nacional)