# **Noticias nacionales con temática educativa** :pencil:
Disfruta del diverso contenido que tenemos para ti:
## Contenido
* [Bachilleres demostraron mejoras en el rendimiento del Ser Bachiller](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Nacional/RENDIMIENTO-SER-BACHILLER.md)
* [Clases gratuitas de ajedrez en la terminal terrestre de Guayaquil](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Nacional/CLASES-DE-AJEDREZ-EN-GUAYAQUIL.md)
* [Santo Domingo tendrá nueva universidad después de 2019](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Nacional/SANTO-DOMINGO-TENDRA-NUEVA-UNIVERSIDAD.md)
* [Madres se graduaron en cuidado infantil](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Nacional/MADRES-SE-GRADUARON-EN-CUIDADO-INFANTIL.md)
* [Nuevos bachilleres de Guayaquil se gradúan](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/blob/master/Educaci%C3%B3n/Educaci%C3%B3n-Nacional/NUEVOS-BACHILLERES-DE-GUAYAQUIL.md)


## [Ir a noticias internacionales acerca de la educación](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/master/Educaci%C3%B3n/Educaci%C3%B3n-Internacional)