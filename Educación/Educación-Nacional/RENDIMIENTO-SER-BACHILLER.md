# **Bachilleres demostraron mejoras en el rendimiento del Ser Bachiller**
***Quito, 16 de agosto de 2018***

Los resultados de la evaluación Ser Bachiller del ciclo Sierra y Amazonía 2017-2018, en promedio, muestran un avance importante en los logros educativos de los estudiantes ecuatorianos.

![imagen-ser-bachiller](https://educacion.gob.ec/wp-content/uploads/2018/08/presi.png)


## Se presenta una tabla del total de inscritos para la aplicación de la prueba

**Población objetivo** | **Presente año lectivo** | **Año lectivo anterior** 
------------ | ------------- | -------------
Población escolar | 99,1% | 94%
Población no escolar | 89,2% | 74,3%

Respecto a la prueba Ser Bachiller hubo una mejora importante en el promedio de la evaluación del ciclo Sierra y Amazonía. En el régimen Sierra-Amazonía 2016-2017 el promedio fue de 7,64 y para el periodo 2017-2018 subió a 7,8. Los estudiantes con un nivel satisfactorio y excelente pasaron del 32,3% al 38,3% en los periodos citados. Se destaca el porcentaje del aumento de los estudiantes excelentes, que pasaron del 0,8% al 2,8%, más del triple. En tanto, los alumnos con insuficiencia disminuyeron de 21,1% al 17,2%.

La mejora educativa ocurrió en todos los campos del conocimiento evaluados: Científico, Matemático, Social y Lingüístico. Contrastando los logros obtenidos en el ciclo Sierra Amazonía 2016-2017 con los actuales 2017-2018, el porcentaje de satisfactorio y excelente tuvo el siguiente comportamiento: Científico, del 27,9% al 34,5%; esto es un incremento de 6,6 puntos. Matemático, del 31,8% al 34,9%; un incremento de 3,1 puntos. Social, del 36,3% al 39,4%; un incremento de 3,1 puntos. Lingüístico del 44,1% al 52,2%; un incremento de 8,1 puntos.

La educación pública ha mejorado notablemente. El porcentaje de satisfactorio y excelente pasa de 24,5% a 32,6%; esto es un aumento de 8,1 puntos. La educación fiscomisional pasa de 35,3% al 39,2%, con un incremento de 3,9 puntos. En cambio, la educación municipal disminuye de 54,1% a 52,7%; una reducción de 1,4 puntos. En la educación particular hay un incremento de 53,1% a 54,2%; esta es un alza de 1,1 puntos.
La brecha de estudiantes evaluados con insuficiente entre la educación pública y privada se redujo de 13 a 8 puntos porcentuales en los dos periodos comparados.

**Fuente:** [Ministerio de Educación](https://educacion.gob.ec/bachilleres-demostraron-mejoras-en-el-rendimiento-del-examen-ser-bachiller/)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Nacional)