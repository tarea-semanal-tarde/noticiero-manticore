# **Nuevos bachilleres de Guayaquil se gradúan**
***Jueves, 1 de marzo de 2018***

Los colegios del sector están viviendo sus ceremonias de graduación de bachilleres. Uno de ellos fue el Alemán Humboldt, que el 22 de febrero pasado graduó a 110 bachilleres de su quincuagésima promoción.

Este año se conmemoró las 50ª promociones de ex alumnos que el plantel ha graduado por medio de un video histórico narrado desde la mirada de los exalumnos de las diversas promociones.

![img-graduacion-guayaquil](https://www.eluniverso.com/sites/default/files/styles/powgallery_1024/public/fotos/2018/02/data22513311.jpg?itok=ASbT5p0V)

El evento contó con la presencia de la consulesa honoraria de Alemania, María Gloria Alarcón, directivos del plantel y de los familiares de los 110 nuevos bachilleres.

Los discursos fueron dados por Katrin Alarcón, rectora del plantel; Ramón Sonnenholzner, presidente de la Asociación Colegio Alemán Humboldt; MSc. Oswaldo Onofre, rector nacional; Dr. David Rodríguez Ycaza, padre de familia, entre otros. Entre las intervenciones musicales estuvo la de la cantante Pamela Cortés.

El martes 6, a las 20:00 en el aula magna de la Universidad Católica, se realizará la ceremonia de graduación de 49 bachilleres de la 28ª promoción de la Unidad Educativa Bilingüe Jefferson.

El mejor bachiller de la promoción es Javier Nivela Dueñas, mientras que los mejores promedios son: Ciencias Exactas, Andrea Wong Sánchez; Ciencias Sociales, Fernanda Jácome; Técnico Aplicaciones Informáticas, Isabella Velarde; Técnico Restaurante y Bar, Helen Wang Jiménez.

El miércoles 7, a las 10:00, en el Teatro Centro de Arte será la ceremonia de graduación de 118 bachilleres de la Unidad Educativa Javier.

En tanto, el colegio Sek gradúa a sus bachilleres el sábado 10 de marzo en los salones del Bankers Club, a las 10:00. Se reconocerá a Ariana Paredes Dillon como mejor bachiller.

**Fuente:** [El universo](https://www.eluniverso.com/guayaquil/2018/03/01/nota/6644575/nuevos-bachilleres-se-graduan)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Nacional)