# **Madres se graduaron en cuidado infantil**
***Domingo, 29 de julio de 2018***

Ciento cinco nuevas tecnólogas en la carrera de Desarrollo Infantil integral se incorporaron al equipo del Ministerio de Inclusión Económica y Social (MIES).

Las egresadas son madres de familia que ahora están profesionalizadas para brindar un cuidado efectivo a los menores de edad.

![graduacion-madre](https://estaticos.elperiodico.com/resources/jpg/8/5/zentauroepp38569393-barcelona-2017-espacio-para-padres-escuela-esc170614185225-1497459340658.jpg)

La graduación se efectuó el jueves pasado en el auditorio del colegio Vicente Rocafuerte de Guayaquil.

Alma Zeballos, representante de la Secretaría de Educación Superior, Ciencia, Tecnología e Innovación, dijo que los títulos en tecnologías serán considerados de tercer nivel.

Martha Castro Gómez, coordinadora de la carrera, dijo que estar a cargo de un grupo de profesionales y compartir con los estudiantes le ha permitido mejorar como ser humano y ser partícipe de todos los logros y triunfos obtenidos de cada estudiante en el aula.

**Fuente:** [El universo](https://www.eluniverso.com/noticias/2018/07/29/nota/6880676/madres-se-graduaron-cuidado-infantil)


[**Regresar al índice**](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/rama-educacion/Educacion/Educacion-Nacional)