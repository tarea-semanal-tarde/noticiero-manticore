# **FARÁNDULA NACIONAL** 🌎

## ***Se encuentra en la sección de farándula nacional, donde encontrará encandalos y  chismes de los famosos de ECUATORIANOS.***

![alt](https://a2-images.myspacecdn.com/images03/1/7ce9f03f8ceb44d897826728cba3331a/300x300.jpg) 
![alt](https://solaz.ec/wp-content/uploads/2017/07/foto-82-300x300.jpg)

![alt](https://i.scdn.co/image/53eeb031b40726e9666c8a647368b1035a2f1e73)

# **Tabla de Contenido :**
 * ## [1. Celebridades en la Vida 👪](articulos-vida-ecuador.md)
   
    *En el 2017 los famosos no dejaron de sorprendernos con todo lo que pasó en sus vidas personales y profesionales, la mayoría de ellos superando a cualquier tipo de ficción hasta ahora contada.* 

    * [1.1 Articulos](articulos-vida-ecuador.md)
 
    * [1.2 Videos: Pablo Northia responde si tiene una relación homosexual](https://www.youtube.com/watch?v=SNgEh8ezSfc)

 * ## [2. Celebridades en la TV y Música 📺](articulos-tv-ecuador.md)
    
    *Aquí encontrarás algunos personajes famosos de la televisión que se han visto envueltos en diferentes casos bochornosos en las redes sociales y en los medios Ecuatorianos.*

    * [2.1 Articulos](articulos-tv-ecuador.md)

    * [2.2 Videos: Escándalo por comprometedoras fotos de Míster Ecuador ](https://www.youtube.com/watch?v=B_gHoM8wPW4)


 * ## [3. Videos de Escandalos 📽️](#links-de-videos-más-reelevantes)

    *Aquí encontraras algunos videos de los escandalos más reelevantes en el mundo del espectáculo, espero te guste.*  

    * [3.1 Links de Videos](#links-de-videos-más-reelevantes)
 
 * ## [4. Ir a las noticias internacionales sobre farándula 📽️](../farándula-internacional/README.md)

 * ## [5. Regresar a la página principal de farándula 📽️](../README.md)

# Links de videos más reelevantes
  * Estos videos son presentados gracias a [*Jarabe de Pico*](https://www.youtube.com/channel/UCx8qh7SHoms4f77kNeyIR2w):
    * [*Actor ecuatoriano publica burlas en redes sociales por la salida de "Combate"- Jarabe de Pico*](https://www.youtube.com/watch?v=PsgsEU1jqds)
    * [*Maltrato físico y psicológico en una de las parejas de la farándula ecuatoriana - Jarabe de Pico*](https://www.youtube.com/watch?v=h3MGuMQDeTo)
    * [*Usuarios en Facebook piden a Thalía que deje de publicar como millennial - Jarabe de Pico*](https://www.youtube.com/watch?v=NfM1weIKPp8)
    * [*Batichica es la primera heroína abiertamente gay - Jarabe de Pico*](https://www.youtube.com/watch?v=iAzP-RnEewM)


   