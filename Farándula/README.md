# **FARÁNDULA** 📸

# Bienvenidos a las noticias mas reelevantes de la Farándula [Nacional](farándula-nacional/README.md) 🇪🇨 e [Internacional ](farándula-internacional/README.md) 🌎

![Imagen Farándula](https://pbs.twimg.com/profile_images/1559891907/farandulaperu.jpg)

## ***Este sitio es de ustedes, donde van a saber sobre todo lo relacionado con sus estrellas de Hollywood favoritas. También encontrarán en nuestro blogs datos sobre:***

  * ### [Películas: Cinefagos](http://www.cinefagos.es/) 🎞️
    [![X-MEN](https://hips.hearstapps.com/es.h-cdn.co/fotoes/images/noticias-cine/x-men-apocalypse-fox-lanza-el-primer-poster-en-movimiento/88548049-1-esl-ES/X-Men-Apocalypse-Fox-lanza-el-primer-poster-en-movimiento.jpg?crop=0.7113502935420744xw:1xh;center,top&resize=100:*)](https://poseidonhd.net/movies/x-men-dias-del-futuro-pasado/)
    [![COCO](https://dejensever.es/wp-content/uploads/2017/12/DEJENSEVER_PLANTILLAPOSTER_coco-100x100.jpg)](https://poseidonhd.net/movies/coco-c/)
    [![IRON MAN](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS70vWoLXa9M2C0ZNckry3THooRFv4IdJvI3Y7WS0faRtXdgq2n)](https://poseidonhd.net/movies/iron-man/)
    

  * ### [Próximos estrenos: El Antepenultimo Mohicano](https://www.elantepenultimomohicano.com/) 📽️
    [![AQUAMAN](https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/po-ster-oficial-aquaman-1531825724.jpg?crop=1.00xw:0.676xh;0,0.199xh&resize=100:*)](https://www.youtube.com/watch?v=OHJShE-31wQ)
    [![THOR](https://i2.wp.com/www.septimacaja.com/wp-content/uploads/thor-ragnarok-poster-main.jpg?resize=100%2C100&ssl=1)](https://poseidonhd.net/movies/thor-ragnarok-h/)
    [![READY PLAYER ONE](https://2.bp.blogspot.com/-B3uhw1F-gsY/Wri6Y9duU5I/AAAAAAAAK5o/4USfwv0_URAY48qJhw6PdxhkXZGwmMTgwCLcBGAs/s100-c/rpo_poster.jpg)](https://poseidonhd.net/movies/ready-player-one-g/)

  * ### [Música: La Música.com ](http://www.lamusica.com.co/blog/) 📀
    [![90 hitS REGGAETON](https://contentpl-a.akamaihd.net/images/playlists/image/small/236431.jpg)](https://www.youtube.com/watch?v=51uvWBcCPDU)
    [![ELECTRONIC](https://i.pinimg.com/236x/ea/1f/b4/ea1fb46e559b4bf6b4e406fc2e9ac4f6--skullgirls-songs.jpg)](https://www.youtube.com/watch?v=fJrQffUIyMM)
    [![REGGAETON ANTIGUO](https://listasspotify.es/wp-content/uploads/2018/05/trap-latino-100x100.jpg)](https://www.youtube.com/watch?v=L4SVtMRdseM)
    


# **¿Qué Noticias quieres ver?** 😎 

 * ### [Farándula-Nacional](farándula-nacional/README.md) 🇪🇨

 * ### [Farándula-Internacional](farándula-internacional/README.md) 🌎

 * ## [Regresar a la página principal📽️](../README.md)

## ***Nota: Si haces click sobre las imagenes te redirigiran a las péliculas y música exhibida***