# **Escandalos y Chismes de la vida de los famosos🤐**

## Noticias Recientes de los Famosos😵
## Índice:
 * [*El día después de la separación de Angelina Jolie y Brad Pitt.*](el-día-después-de-la-separación-de-angelina-jolie-y-brad-pitt.md)
 * [*Tildan a Maluma de machista por su nuevo video y así reacciona su novia Natalía Barulích.*](tildan-a-maluma-de-machista-por-su-nuevo-video-y-así-reacciona-su-novia-natalía-barulích.md)
 * [*Kim y Khloé Kardashian no contienen su ira y atacan al exnovio de Kourtney por Instagram*](kim-y-khloé-kardashian-no-contienen-su-ira-y-atacan-al-exnovio-de-kourtney-por-instagram.md)
 * [*¡Nick Jonas y Priyanka Chopra se han comprometido!.*](nick-jonas-y-priyanka-chopra-se-han-comprometido.md)
 * [*Kofi Annan fallece a los 80 años.*](kofi-annan-fallece-a-los-80-años.md)
 * [*Scarlett Johansson es la actriz mejor pagada de Hollywood.*](scarlett-johansson-es-la-actriz-mejor-pagada-de-hollywood.md)
 

## [Regresar al menú de farándula internacional 📽️](README.md)
## [Ir a las noticias nacionales sobre farándula 📽️](../farándula-nacional/README.md)
