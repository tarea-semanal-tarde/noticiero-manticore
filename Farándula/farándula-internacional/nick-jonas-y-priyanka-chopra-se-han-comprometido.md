# ¡Nick Jonas y Priyanka Chopra se han comprometido!

Sábado, 18 agosto 2018

Nick Jonas y Priyanka Chopra han dado un paso más en su relación. Ayer, la pareja se comprometía siguiendo las tradiciones hindúes; Celebraron la ceremonia Roka, un evento que marca el comienzo, no solo de la unión de la nueva pareja, sino de las familias de cada uno. Las alarmas de su compromiso saltaban hace unos días, cuando una amiga de Priyanka subía a Instagram una foto en la que podía verse un gran anillo en el dedo de la actriz, pero no ha sido hasta ahora cuando ambos lo han confirmado de manera oficial. "Futura señora Jonas. Mi corazón. Mi amor", escribe él junto a una imagen en la que la actriz, cantante y Miss Mundo en los 2000, luce anillo de compromiso. Ella también ha querido compartir la imagen; "Tomado ... Con todo mi corazón y mi alma", escribe.

![Nick](imagenes-farándula-internacional/nick.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22764758/nick-jonas-priyanka-chopra-comprometidos/) 

* [*Regresar al índice*](articulos-vida.md)





