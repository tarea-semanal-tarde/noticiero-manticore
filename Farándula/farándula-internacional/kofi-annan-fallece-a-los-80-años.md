# Kofi Annan fallece a los 80 años

Sábado, 18 agosto 2018

El ex secretario general de Naciones Unidas Kofi Annan ha fallecido a los 80 años de edad, tal y como ha confirmado su familia vía redes sociales. El diplomático ghanés enfermó cuando regresaba de Sudáfrica tras acudir a la conmemoración del aniversario del nacimiento del líder sudafricano Nelson Mandela. Kofi Annan fue hospitalizado a su llegada en un hospital de Ginebra y posteriormente trasladado en avión hasta otro de la capital de Suiza, Berna. "Con inmensa tristeza que la familia Annan y la Fundación Kofi Annan anuncian que Kofi Annan, ex Secretario General de las Naciones Unidas y Premio Nobel de la Paz, falleció pacíficamente el sábado 18 de agosto después de una breve enfermedad ...", rezaba el tuit publicado por la familia del diplomático que fue galardonado en 2011 con el Premio Nobel de la Paz "por su trabajo por un mundo más organizado y pacífico".

![Kann](imagenes-farándula-internacional/kann.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22764048/kofi-annan-fallece/) 

* [*Regresar al índice*](articulos-vida.md)





