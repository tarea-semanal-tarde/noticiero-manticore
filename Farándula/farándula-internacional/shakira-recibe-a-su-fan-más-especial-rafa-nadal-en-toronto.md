# Shakira recibe a su 'fan' más especial, Rafa Nadal, en Toronto

Sábado, 11 agosto 2018

Desde que Shakira supiera que podía volver a cantar, su emoción es palpable cada vez que se sube a los escenarios. Después de superar sus complicaciones de salud por problemas en sus cuerdas vocales por las que tuvo que cancelar su gira 'El dorado', la cantante ha reaparecido con más fuerza que nunca. El tour está llevando a la colombiana a viajar por todo el mundo, y su última parada ha sido Nueva York. Allí Shakira ha desatado, por completo, la locura entre sus seguidores. Desde su llegada al concierto hasta cada canción, con la que el público vibró de inicio a fin. Con un éxito arrollador la artista cumplía con una de las citas más importantes de su gira: cantar en el Madison Square Garden de Nueva York.

![Shakira](imagenes-farándula-internacional/shakira2.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22704051/shakira-concierto-fan-rafa-nadal/) 

* [*Regresar al índice*](articulos-tv.md)





