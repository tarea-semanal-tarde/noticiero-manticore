# Angelina Jolie denuncia a Brad Pitt por no pagar la manutención de sus hijos

Miécoles, 08 agosto 2018

Brad Pitt podría llevar un año y medio sin pagar la pensión alimenticia de sus hijos. Al menos así lo aseguran Angelina Jolie y su abogada, quienes están dispuestas a emprender medidas legales contra el actor. Hace casi dos años desde que se separaron, y parece que no llega la calma para la familia, y parece que no vaya a llegar ahora que Angelina está decidida a llevar la disputa del divorcio y la custodia de sus hijos a los tribunales. Jolie se está poniendo en contacto con la justicia para poner una orden judicial que obligue a Pitt a pagar lo que le corresponde. La abogada de la actriz comentaba que el actor lleva mas de 1 año y medio sin pagar la manutención y que por lo tanto están en su derecho de solicitar una orden judicial.

![Angelina](imagenes-farándula-internacional/angelina2.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22673085/angelina-jolie-denuncia-brad-pitt/) 

* [*Regresar al índice*](articulos-cine.md)

