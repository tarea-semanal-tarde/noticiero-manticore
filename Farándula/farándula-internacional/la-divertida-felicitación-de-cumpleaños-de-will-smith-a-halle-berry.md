# La divertida felicitación de cumpleaños de Will Smith a Halle Berry

Martes, 15 agosto 2018

Un 14 de agosto de 1966, hace exactamente 52 años, nacía la guapísima y talentosa Halle Berry, una de las actrices más cotizadas de Hollywood desde que apareciera en 'Boomerang', junto a Eddie Murphy en 1992, algo que la proclamó la actriz más deseada del año. No hay quien no recuerde su imagen saliendo del mar en 'Muere otro día', donde dio vida a Giacinta 'Jinx' Johnson, una atrevida agente de la NSA. Es por eso que no hay quien no haya querido dedicar unos minutos a felicitar a la actriz por su 52 cumpleaños. Pero entre cariñosos mensajes y tiernas imágenes, nos quedamos con la felicitación de Will Smith, que sin duda ha revolucionado la red.

Ante sus más de 20 millones de seguidores, el actor publicaba una divertida imagen para felicitar a Halle. Se trata de una fotografía que fusiona sus dos caras y el resultado es... Juzguen ustedes mismos.

![Will](imagenes-farándula-internacional/will.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22734417/will-smith-felicita-original-halle-berry/) 

* [*Regresar al índice*](articulos-cine.md)

