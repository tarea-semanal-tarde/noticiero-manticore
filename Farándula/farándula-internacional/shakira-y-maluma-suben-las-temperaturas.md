# Kofi Annan fallece a los 80 años

Sábado, 27 julio 2018

Shakira vive uno de sus mejores momentos. La cantante colombiana no deja de cosechar éxitos musicales, el último ha sido junto al cantante Maluma con quien ha estrenado nuevo videoclip. 'Clandestino' se ha convertido en todo una revolución en la red por su derroche de sensualidad. Este viernes sus fans se han quedado boquiabiertos con unas imágenes de Shakira en bikini en la orilla del mar moviendo sus caderas. Por la cantante no pasan los años y puede presumir de seguir teniendo un cuerpo que quita el hipo. Y Maluma tampoco se queda corto, el de Medellín saca a relucir su lado más sensual bailando alrededor de Shakira que aparece encerrada en una jaula. Un jugueteo entre los dos que desprende puro magnetismo y que en tan sólo unas horas tiene más de medio millón de visualizaciones.

![Shakira](imagenes-farándula-internacional/shakira.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22575753/shakira-maluma-clandestino-videoclip/) 

* [*Regresar al índice*](articulos-tv.md)




