# John Travolta y Olivia Newton-John protagonizan el reencuentro de 'Grease'

Jueves, 16 agosto 2018

En 1978, Danny Zuko y Sandy Olsson enamoraron a toda una generación gracias a su historia de amor en 'Grease'. Ahora, 40 años después, la pareja ha demostrado que todavía tiene el mismo ritmo. Olivia Newton-John y John Travolta se han reunido en el Samuel Goldwyn Theatre de Los Angeles, con motivo de la proyección del 40 aniversario de 'Grease'. La pareja posó para las fotos sonrientes y abrazados, demostrando su buena relación. Los protagonistas de 'Grease' se mostraron muy animados, demostrando que recuerdan los bailes del musical al atreverse con algunos movimientos. Olivia, de 69 años, parecía tan joven como siempre.

![Jonn](imagenes-farándula-internacional/jonn.jpg)
* Fuente: [*DIEZ MINUTOS*](https:https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22743728/john-travolta-olivia-newton-john-reencuentro-grease/) 

* [*Regresar al índice*](articulos-tv.md)


