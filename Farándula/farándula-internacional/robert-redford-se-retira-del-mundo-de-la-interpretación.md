# Robert Redford se retira del mundo de la interpretación

Martes, 07 agosto 2018

El actor estadounidense, Robert Redford, no podía haberse imaginado un mejor final a su carrera como actor. Tras más de seis décadas, dedicándose en cuerpo y alma al mundo de la industria del entretenimiento, el intérprete ha anunciado su retirada de la gran pantalla. Un anuncio de jubilación que el actor ha confesado en su reciente entrevista para la revista estadounidense 'Entertainment Weekly' y que además coincide con el estreno de su último trabajo. Una comedia titulada 'The old man and the gun' que se estrenará el próximo septiembre en los Estados Unidos y en donde el inconfundible actor de melena rubia se pone en la piel de Forrest Tucker, un preso fugado y criminal de profesión.

![Robert](imagenes-farándula-internacional/robert.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22659697/robert-redford-anuncia-retirada-actor/) 

* [*Regresar al índice*](articulos-cine.md)


