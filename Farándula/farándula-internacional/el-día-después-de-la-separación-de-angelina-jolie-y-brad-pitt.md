# El día después de la separación de Angelina Jolie y Brad Pitt

Martes, 23 de septiembre de 2017

Un lugar a dudas la noticia que remeció los cimientos de Hollywood fue el del fin del romance de esta pareja que tan solo en 2014 contraían matrimonio en una ceremonia a la que asistieron sus hijos. Fueron 12 años de relación que la pareja decidió desechar, luego de que un incidente diera pie a que la prensa rosa se interesase en el futuro de la popular pareja conocida como 'Brangelina'. Y es que en el mes de setiembre, un incidente pondría a la pareja en los titulares de cuanto tabloide fuese publicado en Europa. Durante un vuelo entre Francia y los EE. UU. Pitt actuó con violencia contra su esposa e hijos y fue el mayor Maddox quien debió interponerse entre la pareja. Tras una investigación por la policía estadounidense Pitt y Jolie dieron por terminado su matrimonio y ahora luchan por los afectos de sus pequeños y los derechos de tutelaje.

![Bratt y Jolie](imagenes-farándula-internacional/Angelina.jpg)
* Fuente: [*EL PAÍS*](https://elpais.com/elpais/2016/09/20/estilo/1474383080_754958.html) 


* [*Regresar al índice*](articulos-vida.md)

