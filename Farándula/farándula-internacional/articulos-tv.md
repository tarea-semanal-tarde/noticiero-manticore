# **Escandalos y Chismes de los famosos en televisión y música🤐**

## Noticias Recientes de los Famosos😵
## Índice:
 * [*John Travolta y Olivia Newton-John protagonizan el reencuentro de 'Grease'.*](john-travolta-y-olivia-newton-john-protagonizan-el-reencuentro-de-grease.md)
 * [*Robert Redford se retira del mundo de la interpretación.*](robert-redford-se-retira-del-mundo-de-la-interpretación.md)
 * [*Shakira y Maluma suben las temperaturas*](shakira-y-maluma-suben-las-temperaturas.md)
 * [*Kofi Annan fallece a los 80 años.*](kofi-annan-fallece-a-los-80-años.md)
 * [*Shakira recibe a su 'fan' más especial, Rafa Nadal, en Toronto.*](shakira-recibe-a-su-fan-más-especial-rafa-nadal-en-toronto.md)
 

## [Regresar al menú de farándula internacional 📽️](README.md)
## [Ir a las noticias nacionales sobre farándula 📽️](../farándula-nacional/README.md)
