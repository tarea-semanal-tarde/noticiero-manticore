# **Escandalos y Chismes de los famosos en el cine y música🤐**

## Noticias Recientes de los Famosos😵
## Índice:
 * [*La divertida felicitación de cumpleaños de Will Smith a Halle Berry.*](la-divertida-felicitación-de-cumpleaños-de-will-smith-a-halle-berry.md)
 * [*Madonna, 60 años "like a virgin".*](madonna-60-años-like-a-virgin.md)
 * [*Luis Fonsi: "He sido testigo de cómo la música tiene poderes sanadores".*](luis-fonsi-he-sido-testigo-de-cómo-la-música-tiene-poderes-sanadores.md)
 * [*Angelina Jolie denuncia a Brad Pitt por no pagar la manutención de sus hijos.*](Angelina-jolie-denuncia-a-brad-pitt-por-no-pagar-la-manutención-de-sus-hijos.md)
 * [*Demi Lovato cancela su gira de conciertos por Sudamérica.*](demi-lovato-cancela-su-gira-de-conciertos-por-sudamérica.md)
 

## [Regresar al menú de farándula internacional 📽️](README.md)
## [Ir a las noticias nacionales sobre farándula 📽️](../farándula-nacional/README.md)
