# Luis Fonsi: "He sido testigo de cómo la música tiene poderes sanadores"

Sábado, 11 agosto 2018

Pocos podían pensar, ni siquiera su autor, el cantante Luis Fonsi, que 'Despacito' se convertiría no solo en la canción número uno en más de 40 países, sino en la más escuchada de YouTube, con cinco mil millones de reproducciones. Un éxito sin precedentes que le ha convertido en el máximo exponente de la música hispana.

Fonsi nos recibió en Marbella, donde acudió para recoger de manos de María Bravo el Premio a la Solidaridad que cada año otorga la Fundación Global Gift. Con María visitó hace unos semanas un orfanato en Vietnam, lo que según nos confesó le causó un gran impacto, como todo lo que tiene que ver con la infancia. Muy implicado en obras sociales, el cantante puertorriqueño es un hombre cercano y amable, locamente enamorado de la modelo española Águeda López, con la que tiene dos hijos, Mikaela y Rocco.

![Luis](imagenes-farándula-internacional/luis.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22682271/luis-fonsi-entrevista-10/) 

* [*Regresar al índice*](articulos-cine.md)

