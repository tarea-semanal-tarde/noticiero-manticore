# Demi Lovato cancela su gira de conciertos por Sudamérica

Viernes, 10 agosto 2018

Como era de esperar, los acontecimientos de las últimas semanas han afectado de lleno a la gira de conciertos que tenía en marcha Demi Lovato. El pasado 24 de julio, la cantante estadounidense ingresaba en el hospital Cedars-Sinai de Los Ángeles después de que fuera hallada en su casa en estado inconsciente debido a una supuesta sobredosis de heroína. Tras recibir el alta, la joven ha ingresado en un centro de rehabilitación para tratar sus problemas con las drogas. En un primer momento, se especuló con que, aunque no existe un periodo mínimo de estancia en la clínica, Lovato estaría ingresada durante al menos un mes, pero las cosas parecen que van para largo…

La cantante y sus promotores han lanzado un comunicado para anunciar que se cancelan todas las fechas programas de lo que quedaba por delante de su gira mundial ‘Tell Me You Love Me’. De esta manera, América del Sur, de momento, se quedará sin ver a la diva sobre los escenarios. El próximo 20 de septiembre la cantante iba a visitar Ciudad de México (México), así como otras siete ciudades en noviembre: Monterrey (México), Santiago de Chile (Chile), Buenos Aires (Argentina), Sao Paulo (Brasil), Río de Janeiro (Brasil), Olinda (Brasil) y Fortaleza (Brasil).

![DEMI](imagenes-farándula-internacional/demi.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22696013/demi-lovato-cancela-gira-conciertos/) 

* [*Regresar al índice*](articulos-cine.md)

