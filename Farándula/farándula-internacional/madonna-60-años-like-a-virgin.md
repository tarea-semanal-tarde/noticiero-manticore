# Madonna, 60 años "like a virgin"

Miercoles, 16 agosto 2018

Tal día como hoy en 1958 nacía Madonna Louise Veronica Ciccone, una mujer que creció rompiendo esquemas y dispuesta a revolucionarlo todo. Más conocida como Madonna, lleva 36 años 'dando guerra' en el panorama musical internacional, y no hay quien no sepa tararear 'Like a virgin' o no conozca ese 'Hung up' en el que la veíamos bailar con un leotardo rosa en un estudio de ballet. Pero Madonna es mucho más que la reina del pop y una bailarina de excepción... Tras su imagen rebelde y transgresora se esconde un icono feminista y la viva imagen de la solidaridad, aunque también la ha perseguido la polémica. Repasamos su vida en su 60 cumpleaños.

![Madonna](imagenes-farándula-internacional/madonna.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22745814/madonna-60-cumpleanos/) 

* [*Regresar al índice*](articulos-cine.md)

