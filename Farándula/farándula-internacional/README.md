# **FARÁNDULA INTERNACIONAL** 🌎

## ***Se encuentra en la sección de farándula internacional, donde encontrará encandalos y  chismes de los grandes famosos de HOLLYWOOD.***

![alt](https://hips.hearstapps.com/es.h-cdn.co/fotoes/images/noticias-cine/scarlett-johansson-colectivo-trans-hollywood/138129345-1-esl-ES/Por-que-Hollywood-sigue-haciendolo-mal-con-los-personajes-trans.jpg?crop=1xw:1.0xh;center,top&resize=100:*) 
![alt](https://mediamass.net/jdd/public/documents/thumbs/celebrities100/6787.jpg)
![alt](https://i.pinimg.com/736x/2d/f2/10/2df210d02799735123b12a39eb940785--music-books-famous-faces.jpg)

![alt](https://rockntech.com.br/wp-content/uploads/2017/04/cartoons-celebridades_dest.jpg)

![alt](https://imagesvc.timeincapp.com/v3/mm/image?url=https%3A%2F%2Fpeopledotcom.files.wordpress.com%2F2017%2F10%2Fjennifer-aniston4.jpg&w=100&h=100&c=sc&poi=face&q=85)
![alt](https://www.cotilleando.com/data/attachments/546/546966-1571105435b9e19747d476db1102e8b4.jpg)
![alt](https://1yazat78t1q1ticyv3i8g9il-wpengine.netdna-ssl.com/wp-content/uploads/2015/08/Lobo-100x100.jpg)

# **Tabla de Contenido :**
 * ## [1. Celebridades en la Vida 👪](articulos-vida.md)
   
    *En el 2017 los famosos no dejaron de sorprendernos con todo lo que pasó en sus vidas personales y profesionales, la mayoría de ellos superando a cualquier tipo de ficción hasta ahora contada.* 

    * [1.1 Articulos](articulos-vida.md)
 
    * [1.2 Videos: Celebridades con Hijos PROBLEMÁTICOS](https://www.youtube.com/watch?v=Oee634v8RwU)

 * ## [2. Celebridades en la TV y Música 📺](articulos-tv.md)
    
    *Aquí encontrarás algunos personajes famosos de la televisión que se han visto envueltos en diferentes casos bochornosos en las redes sociales y en los medios.*

    * [2.1 Articulos](articulos-tv.md)

    * [2.2 Videos: ESCÁNDALOS de Programas de Televisión de Concursos](https://www.youtube.com/watch?v=B_GyReFnprM)


 * ## [3. Celebridades en el Cine y Música 🍿](articulos-cine.md)
  
    *Aunque Hollywood es un lugar conocido por su glamour y por ser la casa de la industria del cine norteamericano, también se ha llegado a destacar por el constante flujo de escándalos provocados o protagonizados por las estrellas. Por eso, en la llamada época dorada existía un grupo de hombres encargados de resolver las situaciones incómodas en las que los actores y actrices podían llegar a encontrarse. Entérate qué hacían para hacer las turbulencias pasar desapercibidas.*

    * [3.1 Articulos](articulos-cine.md)

    * [3.2 Videos: Escándalos y Disputas de Celebridades del 2017](https://www.youtube.com/watch?v=1hQ2xKPRtRU)


 * ## [4. Videos de Escandalos 📽️](#links-de-videos-más-reelevantes)

    *Aquí encontraras algunos videos de los escandalos más reelevantes en el mundo del espectáculo, espero te guste.*  

    * [4.1 Links de Videos](#links-de-videos-más-reelevantes)
 
 * ## [5. Ir a las noticias nacionales sobre farándula 📽️](../farándula-nacional/README.md)

 * ## [6. Regresar a la página principal de farándula 📽️](../../README.md)
 
# Links de videos más reelevantes
  * Estos videos son presentados gracias a [*WatchMojo*]( https://www.youtube.com/user/watchmojoespanol):
    * [*Top 10 de Escandalos de Youtubers*](https://www.youtube.com/watch?v=bgPJLZpZ2yE)
    * [*Top 10 de Famosos que abusaron de las cirujias*](https://www.youtube.com/watch?v=muRoZ5xHCmg)
    * [*10 Razones por las que Nicki Minaj es ODIADA*](https://www.youtube.com/watch?v=i40rNWfeBFc)
    * [*Top 10 Episodios más RIDÍCULOS de Como Dice el Dicho*](https://www.youtube.com/watch?v=lFi0lJLz3sM)


   