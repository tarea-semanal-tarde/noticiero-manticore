# Scarlett Johansson es la actriz mejor pagada de Hollywood

Viernes, 17 agosto 2018

Su interpretación de la Viuda Negra le ha costado a Scarlett Johansson el puesto de actriz mejor pagada del año. La actriz ha dado vida a personajes tan dispares como Lucy (en 'Lucy'), Cristina (en 'Vicky, Cristina, Barcelona') o aquella inolvidable Grace MacLean en 'El hombre que susurraba a los caballos', película con la que dio el salto a la fama con tan solo 14 años. Ahora Scarlett puede presumir de encabezar la lista de 2018 de las actrices mejor pagadas publicada por la revista 'Forbes', un puesto alcanzado tras su participación en las películas de 'Los Vengadores'. Según las cifras del medio, la actriz se ha embolsado, entre junio de 2017 y junio de 2018, un total de 40,5 millones de dólares, cuatro veces más que el periodo anterior, sustituyendo a Emma Stone en el primer puesto de la popular lista.

![Scarlett](imagenes-farándula-internacional/scarlett.jpg)
* Fuente: [*DIEZ MINUTOS*](https://www.diezminutos.es/famosos-corazon/famosos-extranjeros/a22753558/scarlett-johansson-actriz-mejor-pagada-ano//) 

* [*Regresar al índice*](articulos-vida.md)





