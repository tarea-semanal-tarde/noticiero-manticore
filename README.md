# **Noticiero Manticore 🌐**

## ***BIENVENIDOS A NOTICIERO MANTICORE***
### ***"El Noticiero N°1 del ECUADOR" 🇪🇨***
<br>
<br>

|[EDUCACIÓN](#educación:) 📖 | [TECNOLOGÍA](#tecnología:) 🤖 | [DEPORTES](#deportes:) 🚴| [FARÁNDULA](#farándula:) 📸|
|----------|:-------------:|:-------------:|------:|
<br>
<br>
<br>

# Tabla de Contenidos
## *Nuestro noticiero contiene noticias NACIONALES 🇪🇨 e INTERNACIONALES 🌎 muy reelevantes de la actualidad como:*


### **Educación:📖**    

* ***En la sección educación podrás ver noticias respecto a la educación Nacional e Internacional.***

![Educación1](https://static.vix.com/es/sites/default/files/styles/thumbnail/public/btg/curiosidades.batanga.com/files/7-inolvidables-mujeres-en-la-historia-de-la-educacion.jpg?itok=AYfmrWdh)
![Educación2](https://static.vix.com/es/sites/default/files/styles/thumbnail/public/btg/indigenas-educacion.jpg?itok=rAl6LGw2)
![Educación3](https://direccionnacionaleducacionmediasv.files.wordpress.com/2018/06/cropped-06.jpg?w=200)

 * Noticias de Educación
<br>
[Más noticias de Educación aquí!](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/master/Educaci%C3%B3n)
<br>

### **Tecnología:🤖** 

* ***En la sección tecnología podrás ver noticias respecto a las últimas tendencias tecnologicas en el mundo.***

![Tecnología1](https://static.vix.com/es/sites/default/files/styles/thumbnail/public/r/robots-y-educacion.jpg?itok=5P5ho2hl)
![Tecnología2](https://static.vix.com/es/sites/default/files/styles/thumbnail/public/btg/tech.batanga.com/files/5-errores-de-seguridad-informatica-basados-en-mitos-que-debes-evitar.jpg?itok=JBbcoPLH)
![Tecnología3](https://cerebrodigital.org/images/posts/Como-sabremos-que-la-Inteligencia-Artificial-ha-adquirido-conciencia-thumb.png)

 * Noticias Tecnológicas
<br>
[Más noticias de Tecnología aquí!](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/master/Tecnolog%C3%ADa)
<br>


### **Deportes:🚴**  

* ***En la sección deportes podrás ver noticias respecto a los últimos acontecimientos mundiales deportivos .***

![Deportes1](https://miesquinacaliente.files.wordpress.com/2013/11/yoelkis-cruz-opt.jpg?resize=200%2C200)
![Deportes2](https://ugc.kn3.net/i/origin/http://img.terra.com.mx/galeria_de_fotos/images/67/133214.jpg)
![Deportes3](https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Rudy_Fernandez_Eurobasket_2011.jpg/200px-Rudy_Fernandez_Eurobasket_2011.jpg)

 * Noticias de Deportes
<br>
[Más noticias de Deportes aquí!](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/master/Deportes)
<br>

### **Farándula:📸**  

* ***En la sección farándula podrás ver noticias, escándalos y chismes respecto a la farándula Nacional e Internacional.***

![Deportes3](https://static.vix.com/es/sites/default/files/styles/thumbnail/public/btg/cine.batanga.com/files/los-vengadores-the-avengers-la-pelicula-foto.jpg?itok=MnXX-uby)
![Deportes3](https://gl-images.condecdn.net/image/mP5gBQvKjq9/crop/200/square/f/Gal-Gadot-8dec17-GettyImages-888041954.jpg)
![Deportes3](https://www.thecelebsocial.com/images/players/Hollywood/Scarlett%20Johansson.jpg)

 * Noticias de Farándula
 <br>
[Más noticias de Farándula aquí!](https://gitlab.com/tarea-semanal-tarde/noticiero-manticore/tree/master/Far%C3%A1ndula)
<br>

